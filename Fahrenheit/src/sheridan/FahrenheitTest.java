package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FahrenheitTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void convertFromCelsius() {
		assertTrue("Invalid value for fahrenheixt", 32<=Fahrenheit.convertFromCelsius(27)&&Fahrenheit.convertFromCelsius(27)<5200);
	}

	@Test
	public void convertFromCelsiusBoundaryIn() {
		assertTrue("Invalid value for fahrenheit", 32<=Fahrenheit.convertFromCelsius(0));
	}
	
	@Test
	public void convertFromCelsiusBoundaryOut() {
		assertFalse("Invalid value for fahrenheit", 32<=Fahrenheit.convertFromCelsius(-1));
	}

	@Test
	public void convertFromCelsiusException() {
		assertFalse("Invalid value for fahrenheit", 32<=Fahrenheit.convertFromCelsius(-40));
	}

}
