package sheridan;

public class Fahrenheit {
	public static int convertFromCelsius(int input) {
		  int fahrenheit = (int) Math.ceil((9.0 / 5.0)) * input + 32;
		  return fahrenheit;
	}
	public static void main(String[] args) {
		System.out.println("The 23 celsius converted to fahrenheit is: " + convertFromCelsius(23));
	}

}
